import matplotlib
import matplotlib.pyplot as plt
import numpy as np
num = [1,2,3,4,5,6]
datax_Critic = np.loadtxt("scatter_datax_Critic.txt")
datax_CTopsis = np.loadtxt("scatter_datax_CTopsis.txt")
plt.scatter(num,datax_Critic,c="red",s=200)
plt.scatter(num,datax_CTopsis,c="blue",s=80)
plt.title("Rankings of six plans evaluated by CRITIC and C-TOPSIS")
plt.ylabel('Ranking')
plt.xlabel('Plan')
fig = plt.figure()
ax = fig.add_axes([0.5, 0, 0.5, 1])
ax.text(0.1,0.95, U'图形名称：相位空间图', transform=ax.transAxes,fontdict = {'size': 8, 'color': 'black'})
ax.text(0.1,0.9, U'观察时间：', transform=ax.transAxes,fontdict = {'size': 8, 'color': 'black'})
ax.set_axis_off()
plt.show()

print(datax_Critic,datax_CTopsis)

#解决中文乱码问题
from pylab import *
mpl.rcParams['font.sans-serif'] = ['SimHei']
#新建一个figure对象
fig = plt.figure()
#新建子图，一行两列，第一个
plt.subplot(1, 2, 1)
#画出散点图,x,y代表横纵坐标顺序
t.scatter(x,y, s = 0.1, c = 'r', )
#表明横纵坐标
plt.xlabel('axis_x',fontsize=8)
plt.ylabel('axis_y',fontsize=8)
#新增子区域，假设整个图为(0,0)到y(1,1)，刚刚subplot在（0，0）到（0.5，1）绘图，此时在（0.5，0）到（0.5，1）区域内标注
#不再利用subplot,因为会画上坐标轴
ax = fig.add_axes([0.5, 0, 0.5, 1])
#在（0.5，0）到（0.5，1）区域写入标注，前两个参数是相对位置

ax.set_axis_off()
plt.show()


