import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
from get_distribution import get_data,getmap
from Radarsimulation import calculate_direction
ax = plt.gca()  # 获取到当前坐标轴信息
ax.xaxis.set_ticks_position('top')  # 将y轴的位置设置在右边
ax.invert_yaxis()  # y轴反向
map_boundary = 3500
road_cover = [[0 for i in range(map_boundary)] for j in range(map_boundary)]
for i in range(665,732):
    for j in range(449,1830):
        road_cover[i+800][j+800]=1
for i in range(50,665):
    for j in range(713,783):
        road_cover[i+800][j+800] = 1
for i in range(50,665):
    for j in range(1050,1130):
        road_cover[i+800][j+800] = 1
for i in range(50,665):
    for j in range(1405,1480):
        road_cover[i+800][j+800] = 1
for i in range(50,665):
    for j in range(1763,1830):
        road_cover[i+800][j+800] = 1
for i in range(732,1345):
    for j in range(713,783):
        road_cover[i+800][j+800] = 1
for i in range(732,1345):
    for j in range(1050,1130):
        road_cover[i+800][j+800] = 1
for i in range(732,1345):
    for j in range(1417,1485):
        road_cover[i+800][j+800] = 1
for i in range(732,1345):
    for j in range(1770,1830):
        road_cover[i + 800][j + 800] = 1
data_x,data_y,data_direction=get_data()
map= [[0 for i in range(map_boundary)] for j in range(map_boundary)]
plt.scatter(data_x,data_y,s=30)

map = calculate_direction(np.array(map),data_y[26], data_x[26],data_direction[26])
plt.imshow(road_cover)
plt.imshow(map)
#plt.imshow(road_cover)
plt.show()