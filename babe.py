import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Circle
import random
from matplotlib.patches import Wedge
import xlrd
import xlwt
import pandas as pd
import shapely
import shapely.geometry as geo
import math
import numpy as np
import sys




workbook = xlrd.open_workbook('sbhdx.xls')
excel_sheet = workbook.sheet_by_index(1)

sensor_range = 16
wifi_range = 20


def draw_sensor_range_down(n,m):
    semicircle = Wedge((n,m),sensor_range,0,180,color='black',alpha=0.5)
    ax.add_patch(semicircle)

def draw_sensor_range_left(n,m):
    semicircle = Wedge((n,m),sensor_range,-90,90,color='black',alpha=0.5)
    ax.add_patch(semicircle)

def draw_sensor_range_up(n,m):
    semicircle  = Wedge((n,m),sensor_range,180,360,color='black',alpha=0.5)
    ax.add_patch(semicircle)

def draw_sensor_range_right(n,m):
    semicircle = Wedge((n,m),sensor_range,90,-90,color='black',alpha=0.5)
    ax.add_patch(semicircle)

def draw_wifi_range(n,m):
    circle = Circle(xy=(n,m), radius=wifi_range,color='black',alpha=0.2)
    ax.add_patch(circle)

for ii in range(0,100):
    fig = plt.figure(figsize=(46 * 5, 39 * 5))
    ax = fig.add_subplot(111)
    x = (-22, -22, -7, -5, -9, -14)
    y = (5, 15, 15, 6, 5, 5)
    plt.scatter(x, y, color='black')
    x1 = (-22, -22, -7, -5, -9, -14, -22)
    y1 = (5, 15, 15, 6, 5, 5, 5)
    plt.plot(x1, y1, color='black')
    plt.fill(x1, y1, 'k')

    x2 = (-1, -3, -1, 4, 7)
    y2 = (5, 12, 15, 15, 6)
    plt.scatter(x2, y2, color='black')
    x3 = (-1, -3, -1, 4, 7, -1)
    y3 = (5, 12, 15, 15, 6, 5)
    plt.plot(x3, y3, color='black')
    plt.fill(x3, y3, 'k')

    x4 = (8, 8, 10, 15, 19, 19, 11)
    y4 = (14, 10, 4, 4, 7, 13, 15)
    plt.scatter(x4, y4, color='black')
    x5 = (8, 8, 10, 15, 19, 19, 11, 8)
    y5 = (14, 10, 4, 4, 7, 13, 15, 14)
    plt.plot(x5, y5, color='black')
    plt.fill(x5, y5, 'k')

    x6 = (-5, -5, -5, -5, -8, -18, -21, -19)
    y6 = (1, -1, -11, -16, -18, -18, -17, 3)
    plt.plot(x6, y6, color='black')
    x7 = (-5, -5, -5, -5, -8, -18, -21, -19, -5)
    y7 = (1, -1, -11, -16, -18, -18, -17, 3, 1)
    plt.plot(x7, y7, color='black')
    plt.fill(x7, y7, 'k')

    x8 = (5, 1, 1, 1, 1, 6)
    y8 = (3, 0, -5, -11, -17, -16)
    plt.scatter(x8, y8, color='black')
    x9 = (5, 1, 1, 1, 1, 6, 5)
    y9 = (3, 0, -5, -11, -17, -16, 3)
    plt.plot(x9, y9, color='black')
    plt.fill(x9, y9, 'k')

    x10 = (9, 14, 17, 19, 19, 19, 19, 10, 8, 8)
    y10 = (2, 2, 2, 2, -5, -12, -16, -18, -10, -5)
    plt.scatter(x10, y10, color='black')
    x11 = (9, 14, 17, 19, 19, 19, 19, 10, 8, 8, 9)
    y11 = (2, 2, 2, 2, -5, -12, -16, -18, -10, -5, 2)
    plt.plot(x11, y11, color='black')
    plt.fill(x11, y11, 'k')

    x12 = (-5, -11, -17, -22, -25, -25, -25, -25, -25, -25, -25, -25, -12, -2, 5, 16, 10, 4)
    y12 = (18, 17, 17, 17, 15, 12, 9, 4, -2, -8, -14, -17, -19, -18, -20, -20, 18, 18)
    plt.scatter(x12, y12, color='black')
    x13 = (-5, -11, -17, -22, -25, -25, -25, -25, -25, -25, -25, -25, -12, -2, 5, 16, 19, 19, 19, 19, 19, 19, 10, 4,
           -5)  # 10,19,19,19,19,19,19,4,-5)
    y13 = (18, 17, 17, 17, 15, 12, 9, 4, -2, -8, -14, -17, -19, -18, -20, -20, -16, -12, -5, 2, 7, 13, 18, 18,
           18)  # 18,13,7,2,-5,-12,-16,18,18)
    plt.plot(x13, y13, color='black')


    nrows_num = excel_sheet.nrows
    ncols_num = excel_sheet.ncols
    random_num_right = random.randint(1,16)
    n = excel_sheet.row(random_num_right)[0].value
    m = excel_sheet.row(random_num_right)[1].value
    draw_sensor_range_left(n,m)

    random_num_left = random.randint(17,34)
    n = excel_sheet.row(random_num_left)[0].value
    m = excel_sheet.row(random_num_left)[1].value
    draw_sensor_range_right(n,m)

    random_num_down = random.randint(35,48)
    n = excel_sheet.row(random_num_down)[0].value
    m = excel_sheet.row(random_num_down)[1].value
    draw_sensor_range_up(n,m)

    random_num_up = random.randint(49,56)
    n = excel_sheet.row(random_num_up)[0].value
    m = excel_sheet.row(random_num_up)[1].value
    draw_sensor_range_down(n,m)



    plt.grid()
    plt.plot()
    a=str("pics\\figure"+str(ii)+".png")
    plt.savefig(a)
    plt.close()
    plt.clf()
    print("ok")



