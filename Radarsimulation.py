import math
import numpy as np
import matplotlib.pyplot as plt
map_boundary = 4000
radius = 593
def rotation(two_demensional_array=[]):
    for i in range(len(two_demensional_array)):
        for j in range(i,len(two_demensional_array)):
            temp = two_demensional_array[i][j]
            two_demensional_array[i][j] = two_demensional_array[j][i]
            two_demensional_array[j][i] = temp
def calculate_direction(mapp,x,y,direction):
    if direction == 1:##up
        for i in range(x-radius,x):
            for j in range(y-radius,y+radius):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius:
                    mapp[i][j]+=1

    if direction == 3: #down
        for i in range(x,x+radius):
            for j in range(y-radius,y+radius):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius:
                    mapp[i][j]+=1

    if direction == 2: #left
        for i in range(x-radius,x+radius):
            for j in range(y-radius,y):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius:
                    mapp[i][j]+=1

    if direction == 4: #right
        for i in range(x-radius,x+radius):
            for j in range(y,y+radius):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius:
                    mapp[i][j]+=1
    if direction == 5: #right
        for i in range(x-radius,x+radius):
            for j in range(y-radius,y+radius):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius:
                    mapp[i][j]+=1


    return np.array(mapp)

