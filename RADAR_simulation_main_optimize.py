import xlrd
import numpy as np
import math
import matplotlib
import matplotlib.pyplot as plt
from get_distribution import getmap,get_data
from Radarsimulation import calculate_direction

map_boundary = 3500
ax = plt.gca()  # 获取到当前坐标轴信息
ax.xaxis.set_ticks_position('top')  # 将y轴的位置设置在右边
ax.invert_yaxis()  # y轴反向

data_x,data_y,data_direction=get_data()

map= [[0 for i in range(map_boundary)] for j in range(map_boundary)]
road_cover = [[0 for i in range(map_boundary)] for j in range(map_boundary)]
for i in range(665, 732):
    for j in range(449, 1830):
        road_cover[i+800][j+800]=1
for i in range(50, 665):
    for j in range(713, 783):
        road_cover[i+800][j+800]=1
for i in range(50, 665):
    for j in range(1050, 1130):
        road_cover[i+800][j+800]=1
for i in range(50, 665):
    for j in range(1405, 1480):
        road_cover[i+800][j+800]=1
for i in range(50, 665):
    for j in range(1763, 1830):
        road_cover[i+800][j+800]=1
for i in range(732, 1345):
    for j in range(713, 783):
        road_cover[i+800][j+800]=1
for i in range(732, 1345):
    for j in range(1050, 1130):
        road_cover[i+800][j+800]=1
for i in range(732, 1345):
    for j in range(1417, 1485):
        road_cover[i+800][j+800]=1
for i in range(732, 1345):
    for j in range(1770, 1830):
        road_cover[i+800][j+800]=1
radius = 800
maxx = -1
#plt.scatter(100,200,s=30)
#plt.show()

def area(map,x,y,direction):
    cnt=0
    if direction == 1:##up
        for i in range(x-radius,x):
            for j in range(y-radius,y+radius):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius and road_cover[i][j]:
                    cnt+=1
    if direction == 3: #down
        for i in range(x,x+radius):
            for j in range(y-radius,y+radius):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius and road_cover[i][j]:
                    cnt+=1
    if direction == 2: #left
        for i in range(x-radius,x+radius):
            for j in range(y-radius,y):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius and road_cover[i][j]:
                    cnt+=1
    if direction == 4: #right
        for i in range(x-radius,x+radius):
            for j in range(y,y+radius):
                if math.sqrt(abs(i-x)**2+abs(j-y)**2)<=radius and road_cover[i][j]:
                    cnt += 1
    return int(cnt)


'''
#31 choose 1

maxx=-1
pointa = 0
for cnt in range(0,len(data_direction)):
    map2 = calculate_direction(np.array(map),data_y[cnt],data_x[cnt],data_direction[cnt])
    #scan map2########
    #plt.imshow(map2)
    #plt.show()
    are = area(map2,data_y[cnt],data_x[cnt],data_direction[cnt])
    print(are, cnt)
    if are > maxx:
        maxx = are
        pointa = cnt
print("point:",pointa+1,"area:",maxx)


maxx=-1
pointa,pointb = 0,0
#choose 2
for cnt in range(0,len(data_direction)):
    map2 = calculate_direction(np.array(map), data_y[cnt], data_x[cnt], data_direction[cnt])
    #map = [[0 for i in range(map_boundary)] for j in range(map_boundary)]
    #plt.imshow(map2)
    #plt.show()
    for cnt2 in range(cnt+1,len(data_direction)):
        map3 = calculate_direction(np.array(map2), data_y[cnt2], data_x[cnt2], data_direction[cnt2])
        #plt.imshow(map3)
        #plt.show()
        are = area(map3,data_y[cnt2],data_x[cnt2],data_direction[cnt2])
        print(are, cnt, cnt2)
        if are > maxx:
            maxx = are
            pointa = cnt
            pointb = cnt2
print("points:",pointa,pointb,"area:",maxx)
'''

#choose 3
maxx=-1
pointa,pointb,pointc=0,0,0
for cnt in range(0,len(data_direction)):
    map2 = calculate_direction(np.array(map), data_y[cnt], data_x[cnt], data_direction[cnt])
    #map = [[0 for i in range(map_boundary)] for j in range(map_boundary)]
    #plt.imshow(map2)
    #plt.show()
    for cnt2 in range(cnt+1,len(data_direction)):
        map3 = calculate_direction(np.array(map2), data_y[cnt2], data_x[cnt2], data_direction[cnt2])
        for cnt3 in range(cnt2 + 1, len(data_direction)):
            map4 = calculate_direction(np.array(map3),data_y[cnt3], data_x[cnt3],data_direction[cnt3])
            #plt.imshow(map4)
            #plt.show()
            #迭代面积
            are = area(map4,data_y[cnt3],data_x[cnt3],data_direction[cnt3])
            print(are,cnt,cnt2,cnt3)
            if are > maxx:
                maxx = are
                pointa,pointb,pointc=cnt,cnt2,cnt3


#print(maxx,pointa,pointb,pointc)



#np.savetxt("map.txt",map,fmt='d')

