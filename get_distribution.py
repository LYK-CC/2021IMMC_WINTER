import xlrd
map_boundary = 4000
filename = "/code_pro2/locanew.xls"

def getmap():
    workbook = xlrd.open_workbook(filename,on_demand=True)
    page = workbook.sheet_by_index(0)

    map = [[0 for i in range(map_boundary)] for j in range(map_boundary)]

    row = page.col(1)
    data_x = []
    for i in row:
        if str(i)[0:6] == 'number':
            data_x.append(round(float(str(i)[7:-1])))


    row = page.col(2)
    data_y = []
    for i in row:
        if str(i)[0:6] == 'number':
            data_y.append(round(float(str(i)[7:-1])))

    #绘制txt散点
    #np.savetxt("save.txt",map,fmt='%d')
    for i in range(len(data_x)):
        map[data_x[i]][data_y[i]]=1

    #plt.scatter(data_x, data_y, s=30)
    #plt.show()
    return map

def get_data():
    workbook = xlrd.open_workbook(filename)
    page = workbook.sheet_by_index(0)

    row = page.col(1)
    data_x = []
    for i in row:
        if str(i)[0:6] == 'number':
            data_x.append(round(float(str(i)[7:-1])))


    row = page.col(2)
    data_y = []
    for i in row:
        if str(i)[0:6] == 'number':
            data_y.append(round(float(str(i)[7:-1])))


    # 绘制txt散点
    # np.savetxt("save.txt",map,fmt='%d')

    row = page.col(3)
    data_direction = []
    for i in row:
        if str(i)[0:6] == 'number':
            data_direction.append(round(float(str(i)[7:-1])))


    return data_x,data_y,data_direction

