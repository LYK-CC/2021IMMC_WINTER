#include<bits/stdc++.h>
using namespace std;
const int map_boundary=3000;
const int lamppost=30;
const int radius=593;


int poa[100],pob[100],poc[100],pod[100]; 
int optimum_result=0;
int map_1[map_boundary][map_boundary];
int map_2[map_boundary][map_boundary];
int map_3[map_boundary][map_boundary];
int map_4[map_boundary][map_boundary];
int map_5[map_boundary][map_boundary];
int map_6[map_boundary][map_boundary];
int map_calculate_overlap[map_boundary][map_boundary];
int map_calcu[map_boundary][map_boundary];
int data_x[lamppost+3],data_y[lamppost+3],data_direction[lamppost+3];

int calculate_direction(int x,int y,int direction){
	if(direction==1){
	for(int i=x-radius;i<x;++i)
	for(int j=y-radius;j<y+radius;++j)
	if(sqrt(abs(i-x)*abs(i-x)+abs(j-y)*abs(j-y))<=radius)
	++map_calcu[i][j];
	}
	if(direction==3){
	for(int i=x;i<x+radius;++i)
	for(int j=y-radius;j<y+radius;++j)
	if(sqrt(abs(i-x)*abs(i-x)+abs(j-y)*abs(j-y))<=radius)
	++map_calcu[i][j];
	}
	if(direction==2){
	for(int i=x-radius;i<x+radius;++i)
	for(int j=y-radius;j<y;++j)
	if(sqrt(abs(i-x)*abs(i-x)+abs(j-y)*abs(j-y))<=radius)
	++map_calcu[i][j];
	}
	if(direction==4){
	for(int i=x-radius;i<x+radius;++i)
	for(int j=y;j<y+radius;++j)
	if(sqrt(abs(i-x)*abs(i-x)+abs(j-y)*abs(j-y))<=radius)
	++map_calcu[i][j];
	}
	if(direction==5){
	for(int i=x-radius;i<x+radius;++i)
	for(int j=y-radius;j<y+radius;++j)
	if(sqrt(abs(i-x)*abs(i-x)+abs(j-y)*abs(j-y))<=radius)
	++map_calcu[i][j];
	}
	return 0;
}


int area2(){
    int cont=0;
    for(int i=0;i<map_boundary;++i){
	for(int j=0;j<map_boundary;++j){
	if (map_calcu[i][j]>=2){
	cont+=map_calcu[i][j]-1;
			} 
		}
	}
	return cont;
}


int area(){
    int cont=0;
    for(int i=665;i<732;++i)
	for(int j=449;j<1830;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;
	for(int i=50;i<665;++i)
    for(int j=713;j<783;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;
	for(int i=50;i<665;++i)
    for(int j=1050;j<1130;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;       
    for(int i=50;i<665;++i)
    for(int j=1405;j<1480;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;  
     for(int i=50;i<665;++i)
    for(int j=1763;j<1830;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;    
 	for(int i=732;i<1345;++i)
    for(int j=713;j<783;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;  
 	for(int i=732;i<1345;++i)
    for(int j=1050;j<1130;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;  
 	for(int i=732;i<1345;++i)
    for(int j=1417;j<1485;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;  
 	for(int i=732;i<1345;++i)
    for(int j=1770;j<1830;++j)
    if (map_calcu[i+radius][j+radius]>=1)
    ++cont;
	return cont;
}

int main(){
	
	freopen("data_x.txt","r",stdin);
	for(int i=0;i<lamppost;++i)cin>>data_x[i];
	fclose(stdin);
	freopen("data_y.txt","r",stdin);
	for(int i=0;i<lamppost;++i)cin>>data_y[i];
	fclose(stdin);
	freopen("data_direction.txt","r",stdin);
	for(int i=0;i<lamppost;++i)cin>>data_direction[i];
	
	
	int maxx=-1;
	int pointa=0,pointb=0,pointc=0,pointd=0,pointe=0;
	for(int cnt=0;cnt<lamppost;++cnt){
		cout<<cnt<<endl;
	memcpy(map_calcu,map_1,sizeof(map_1));
	calculate_direction(data_y[cnt], data_x[cnt], 5);
	memcpy(map_2,map_calcu,sizeof(map_calcu));
	for(int cnt2=cnt+1;cnt2<lamppost;++cnt2){
	memcpy(map_calcu,map_2,sizeof(map_calcu));
	calculate_direction(data_y[cnt2], data_x[cnt2], 5);
	memcpy(map_3,map_calcu,sizeof(map_calcu));
	for(int cnt3=cnt2+1;cnt3<lamppost;++cnt3){
	memcpy(map_calcu,map_3,sizeof(map_calcu));
	calculate_direction(data_y[cnt3], data_x[cnt3], 5);
	//memcpy(map_4,map_calcu,sizeof(map_calcu)); 
	//for(int cnt4=cnt3+1;cnt4<lamppost;++cnt4){
	//memcpy(map_calcu,map_4,sizeof(map_calcu));
	//calculate_direction(data_y[cnt4], data_x[cnt4], 5);
	//calculate_direction(data_y[cnt4], data_x[cnt4], data_direction[cnt4]+1%4+1);
	//memcpy(map_5,map_calcu,sizeof(map_calcu));
	//for(int cnt5=cnt4+1;cnt5<lamppost;++cnt5){
	//memcpy(map_calcu,map_5,sizeof(map_calcu));
	//calculate_direction(data_y[cnt5], data_x[cnt5], data_direction[cnt5]);
	int count = area();
	//printf("%d %d %d %d %d\n",count,cnt,cnt2,cnt3,cnt4);
	if(count>maxx){
	maxx=count;
	pointa=cnt,pointb=cnt2,pointc=cnt3;
						}
	if(count==442521){
	cout<<endl<<endl<<endl;
	poa[optimum_result]=cnt;
	pob[optimum_result]=cnt2;
	poc[optimum_result]=cnt;
	++optimum_result;
	cout<<cnt<<" "<<cnt2<<" "<<cnt3<<endl;
						}
					//}
				//}
			}
		}
	}
	cout<<maxx<<endl<<endl;
	int minn=INT_MAX;
	cout<<endl;
	for(int i=0;i<optimum_result;++i){
		printf("%d %d %d\n",poa[i],pob[i],poc[i]);
		memcpy(map_calcu,map_calculate_overlap,sizeof(map_calcu));
		calculate_direction(data_y[poa[i]], data_x[poa[i]], 5);
		calculate_direction(data_y[pob[i]], data_x[pob[i]], 5);
		calculate_direction(data_y[poc[i]], data_x[poc[i]], 5);
		int cnt=area2();
		if(cnt<minn){
		minn=min(minn,cnt);
		pointa=poa[i],pointb=pob[i],pointc=poc[i];
		}
	}
	cout<<pointa<<" "<<pointb<<" "<<pointc<<" "<<minn<<endl;
	
	return 0;
}
