import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpathes
from matplotlib.patches import Wedge
from matplotlib.patches import Ellipse, Circle
from get_distribution import get_data,getmap
from Radarsimulation import calculate_direction
from Radarsimulation2 import calculate_direction2
def draw_sensor_range_down(n,m):
    semicircle = Wedge((n,m),sensor_range,0,180,color='black',alpha=0.5)
    ax.add_patch(semicircle)

def draw_sensor_range_left(n,m):
    semicircle = Wedge((n,m),sensor_range,-90,90,color='black',alpha=0.5)
    ax.add_patch(semicircle)

def draw_sensor_range_up(n,m):
    semicircle  = Wedge((n,m),sensor_range,180,360,color='black',alpha=0.5)
    ax.add_patch(semicircle)

def draw_sensor_range_right(n,m):
    semicircle = Wedge((n,m),sensor_range,90,-90,color='black',alpha=0.5)
    ax.add_patch(semicircle)

def draw_wifi_range(n,m):
    circle = Circle(xy=(n,m), radius=wifi_range,color='black',alpha=0.2)
    ax.add_patch(circle)


map_boundary = 3500
sensor_range = 16
wifi_range = 20
data_x=[]
data_y=[]

#fig = plt.figure(figsize=(46 * 5, 39 * 5))
#ax = fig.add_subplot(111)
ax = plt.gca()  # 获取到当前坐标轴信息
ax.xaxis.set_ticks_position('top')  # 将y轴的位置设置在右边
ax.invert_yaxis()  # y轴反向

road_cover = [[0 for i in range(map_boundary)] for j in range(map_boundary)]
for i in range(665,762):
    for j in range(449,1830):
        data_x.append(i + 800)
        data_y.append(j + 800)
for i in range(50,665):
    for j in range(713,783):
        data_x.append(i + 800)
        data_y.append(j + 800)
for i in range(50,665):
    for j in range(1050,1130):
        data_x.append(i + 800)
        data_y.append(j + 800)
for i in range(50,665):
    for j in range(1405,1480):
        data_x.append(i + 800)
        data_y.append(j + 800)
for i in range(50,665):
    for j in range(1763,1830):
        data_x.append(i + 800)
        data_y.append(j + 800)
for i in range(732,1345):
    for j in range(713,783):
        data_x.append(i + 800)
        data_y.append(j + 800)
for i in range(732,1345):
    for j in range(1050,1130):
        data_x.append(i + 800)
        data_y.append(j + 800)
for i in range(732,1345):
    for j in range(1417,1485):
        data_x.append(i + 800)
        data_y.append(j + 800)
for i in range(732,1345):
    for j in range(1770,1830):
        data_x.append(i+800)
        data_y.append(j+800)
        #plt.scatter([i + 800], [j + 800], s=30, alpha=0.25)
plt.scatter(data_y,data_x,alpha=0.002,s=1,c="black")


data_x,data_y,data_direction=get_data()
map = [[2 for i in range(map_boundary)] for j in range(map_boundary)]
#map=calculate_direction(map,data_y[4],data_x[4],data_direction[4])
#map=calculate_direction(map,data_y[9],data_x[9],data_direction[9])
#map=calculate_direction(map,data_y[11],data_x[11],data_direction[11])
#map=calculate_direction(map,data_y[14],data_x[14],data_direction[14])
#map=calculate_direction(map,data_y[18],data_x[18],data_direction[18])
#map=calculate_direction(map,data_y[24],data_x[24],data_direction[24])
#map=calculate_direction(map,data_y[28],data_x[28],data_direction[28])
map=calculate_direction(map,data_y[7],data_x[7],5)
map=calculate_direction(map,data_y[15],data_x[15],5)
map=calculate_direction(map,data_y[18],data_x[18],5)
map=calculate_direction(map,data_y[29],data_x[29],5)

#ellipse = mpathes.Ellipse(xy4,0.4,0.2,color='y')
#ax.add_patch(ellipse)
#map=calculate_direction2(map,data_y[28],data_x[28],data_direction[28])
plt.imshow(map,alpha=0.7)
#plt.imshow(map2,alpha=0.1)



plt.scatter(data_x,data_y,s=50)
plt.scatter([data_x[7],data_x[15],data_x[18],data_x[29]],[data_y[7],data_y[15],data_y[18],data_y[29]],s=150,c="green")


#plt.xlim(0, 15)
#plt.ylim(0, 15)
#plot_circle((5, 5),r=3)
#road_cover=calculate_direction(100,100,)
#plt.imshow(map)
#plt.imshow(road_cover)
#plt.scatter([100],[100],s=30,alpha=0.25)
#plt.show()
