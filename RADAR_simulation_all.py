import xlrd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from get_distribution import getmap,get_data
from Radarsimulation import calculate_direction


ax = plt.gca()  # 获取到当前坐标轴信息
ax.xaxis.set_ticks_position('top')  # 将y轴的位置设置在右边
ax.invert_yaxis()  # y轴反向

map=getmap()
data_x,data_y,data_direction=get_data()

cnt=0

for cnt in range(0,len(data_direction)):
    map=calculate_direction(np.array(map),data_y[cnt],data_x[cnt],data_direction[cnt])
    print(cnt)

plt.imshow(map)
plt.show()
#np.savetxt("map.txt",map,fmt='d')

