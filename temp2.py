from get_distribution import getmap,get_data
import numpy as np
map = getmap()
np.savetxt("map.txt",map,fmt='%d')
data_x,data_y,data_direction=get_data()
np.savetxt("data_x.txt",data_x,fmt='%d')
np.savetxt("data_y.txt",data_y,fmt='%d')
np.savetxt("data_direction.txt",data_direction,fmt='%d')